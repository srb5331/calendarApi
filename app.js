/*
 * Copyright (c) Microsoft All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */
const express = require('express')
var auth = require('./auth');
var graph = require('./graph');

const app = express()

//server.start(router.route, handle);
app.get('/', function (req, res) {
	res.send('Hello World!')
});

// Get events for a user
app.get('/getCalendarEvents', function (request, response){
	// Get an access token for the app.
	auth.getAccessToken(request, response, function(error, token) {
	 	graph.getUsers(token, request, response, function(error, resp) {
	 		if (error){
	 			response.writeHead(400, {'Content-Type': 'application/json; charset=UTF-8'});
				response.write(JSON.stringify({'error' : error}));
				response.end();
	 		} else {
				response.writeHead(200, {'Content-Type': 'application/json; charset=UTF-8'});
				response.write(JSON.stringify(resp));
				response.end();
			}
		});
})});

app.listen(3000, function () { console.log('Example app listening on port 3000!')})
/*
// Get an access token for the app.
function getCalendars(response, request) {
	response.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
	users2 = "";
	auth.getAccessToken().then(function (token) {
	  // Get all of the users in the tenant.
	  graph.getUsers(token)
	    .then(function (users) {
	    	console.log(users)
	    	users2 = users;
	    	response.write('Users: ' + users2);
	    	//response.write('Hello!' + users);
	    }, function (error) {
	      console.error('>>> Error getting users: ' + error);
	    });
	}, function (error) {
	  console.error('>>> Error getting access token: ' + error);
	});
  	response.end();
}


*/